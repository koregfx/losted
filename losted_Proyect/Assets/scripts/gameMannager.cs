﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class gameMannager : MonoBehaviour 
{

private static gameMannager mannager;
public static gameMannager Mannager
{
	get
	{
		if(mannager == null)
		{
			mannager = GameObject.Find("GameMannager").GetComponent<gameMannager>();
		}
		return mannager;
	}
}
public bool paused;
public float timeSec;
public float timeMin;
private float timeFinal;
public GameObject pauseMenu;
private int nivelScene;
public GameObject nivel;
public bool enJuego;
public bool finNivel;
private bool actuacion1Time;

[Header("Objectos del Menu Final")]
public GameObject menuFinal;
public GameObject estrella1;
public GameObject estrella2;
public GameObject estrella3;
public GameObject iconoTiempo;
public GameObject iconoColeccionable;
public GameObject tiempoText;

[Header("tiempos Menu")]
public float[] tiempoObjetivo;
private GameObject[] engranaje;
public float velocidadRotEngranages;
public bool menuSalido;
private bool rotaEngranaje = true;
public bool colecionableCogido;
public GameObject uiColeccionable;
public GameObject flechaAyuda;
public  bool ayudaFlecha;
public GameObject Opciones;
private GameObject music1;
private GameObject music2;
public bool fixPausa;
public bool cambioNivelMusica;
public GameObject fuegosArtificiales;
private GameObject salida;

	void Start () 
	{
		nivel = GameObject.FindGameObjectWithTag("nivel");
		salida = GameObject.FindGameObjectWithTag("salida");
		engranaje = GameObject.FindGameObjectsWithTag("engranaje");
		Opciones = GameObject.FindGameObjectWithTag("opciones");
		music1 = GameObject.FindGameObjectWithTag("musica");
		music2 = GameObject.FindGameObjectWithTag("musicados");
		actuacion1Time = false;
		enJuego = true;
		finNivel = false;
		StartCoroutine(cuentaTiempo());
		StartCoroutine(giroEngranajes());
		menuSalido = false;	
		fixPausa = false;
		cambioNivelMusica = false;
	}
	
	void Update () 
	{

		if(SceneManager.GetActiveScene().buildIndex >= 8)
		{
			music2.GetComponent<AudioSource>().enabled = true;
			music1.GetComponent<AudioSource>().enabled = false;
			music2.GetComponent<AudioSource>().volume = Opciones.GetComponent<opciones>().volMusic;
			
		}
		else
		{
			music1.GetComponent<AudioSource>().enabled = true;
			music2.GetComponent<AudioSource>().enabled = false;
			music1.GetComponent<AudioSource>().volume = Opciones.GetComponent<opciones>().volMusic;
		}
		
		ayudaFlecha = Opciones.GetComponent<opciones>().ayudaFlecho;
		if(ayudaFlecha && SceneManager.GetActiveScene().buildIndex >2)
		{
			flechaAyuda.SetActive(true);
		}
		if(!ayudaFlecha && SceneManager.GetActiveScene().buildIndex >2)
		{
			flechaAyuda.SetActive(false);
		}
		if(Input.GetKeyDown(KeyCode.Escape) && !fixPausa)
		{
			paused = true;
			fixPausa = true;
		}
		if(Input.GetKeyDown(KeyCode.Escape) && !SceneManager.GetSceneByBuildIndex(2).isLoaded && !fixPausa)
		{
			paused = false;
			fixPausa = true;	
		}
		
		fixPausa = false;
		if(!paused)
		{
			pauseMenu.SetActive(false);
		}
		else if(paused && !finNivel)
		{
			pauseMenu.SetActive(true);
		}
		if(finNivel && !actuacion1Time)
		{	
			//Time.timeScale = 0;
			timeFinal = timeSec + timeMin * 60;
			StartCoroutine(menuFinalTiming());
			actuacion1Time = true;
		}

		if(rotaEngranaje)
		{
			for(int i=0; i < engranaje.Length; i ++)
			{
				engranaje[i].transform.rotation = Quaternion.Lerp(engranaje[i].transform.rotation, engranaje[i].transform.rotation * new Quaternion (0,0,15,1), velocidadRotEngranages * Time.deltaTime);
				
			}

		}

		if(colecionableCogido)
		{
			
			uiColeccionable.GetComponent<Animator>().SetBool("encontrado",true);
		}else
		{
			if(SceneManager.GetActiveScene().buildIndex == 4 || SceneManager.GetActiveScene().buildIndex == 7 || SceneManager.GetActiveScene().buildIndex == 11)
			{
				uiColeccionable.GetComponent<Animator>().SetBool("encontrado",false);
				
			}
		}
	}
	IEnumerator cuentaTiempo()
	{
		while(enJuego)
		{
			if(!paused && !finNivel)
			{
				timeSec += 1;
				if(timeSec == 60)
				{
					timeMin += 1f;
					timeSec = 0;	
				}
			}		
			
			yield return new WaitForSeconds (1f);
		}
		
	}
	IEnumerator menuFinalTiming()
	{
		Opciones.GetComponent<opciones>().nivelesAbiertos = SceneManager.GetActiveScene().buildIndex + 1 - 4;
		menuFinal.SetActive(true);
		yield return new WaitForSeconds (0.75f);
		
		iconoTiempo.SetActive(true);
		yield return new WaitForSeconds (0.5f);

		tiempoText.SetActive(true);
		yield return new WaitForSeconds (0.75f);

		
		if(colecionableCogido)
		{
			if(SceneManager.GetActiveScene().buildIndex == 4 || SceneManager.GetActiveScene().buildIndex == 7 || SceneManager.GetActiveScene().buildIndex == 11)
			{	
				iconoColeccionable.SetActive(true);
				iconoColeccionable.GetComponent<Animator>().SetBool("encontrado",true);
			}	
		}else
		{
			if(SceneManager.GetActiveScene().buildIndex == 4 || SceneManager.GetActiveScene().buildIndex == 7 || SceneManager.GetActiveScene().buildIndex == 11)
			{
				iconoColeccionable.SetActive(true);
				iconoColeccionable.GetComponent<Animator>().SetBool("encontrado",false);
			}
		}
	
		yield return new WaitForSeconds (0.5f);


		if(timeFinal < tiempoObjetivo[2])
		{
			estrella1.SetActive(true);
		}
		yield return new WaitForSeconds (0.5f);

		
		if(timeFinal < tiempoObjetivo[1])
		{
			estrella2.SetActive(true);
		}
		yield return new WaitForSeconds (0.5f);
		
		if(timeFinal < tiempoObjetivo[0])
		{
			estrella3.SetActive(true);
		}
		menuSalido = true;
		yield return 0;
	}
	IEnumerator giroEngranajes()
	{
		
		while(enJuego)
		{
			/*for(int i=0; i < engranaje.Length; i ++)
			{
				engranaje[i].transform.rotation = Quaternion.Slerp(engranaje[i].transform.rotation, engranaje[i].transform.rotation * new Quaternion (0,0,60,0), velocidadRotEngranages * Time.deltaTime);
			}*/

			yield return new WaitForSeconds (0.7f); 

			rotaEngranaje = false;

			yield return new WaitForSeconds (0.3f); 

			rotaEngranaje = true;
		}
	}
	
	public void cambiarEscena(int i)
	{
		Time.timeScale = 1;
		SceneManager.LoadScene(i,LoadSceneMode.Single);
	}
	public void cambiarOpciones()
	{
		SceneManager.LoadScene(2,LoadSceneMode.Additive);
	}
	public void salirOpciones()
	{
		SceneManager.UnloadSceneAsync(2);
	}

	public void cambioEscenaNiveles()
	{
		Time.timeScale = 1;
		SceneManager.LoadScene(nivelScene,LoadSceneMode.Single);
	}

	public void cambioNivel(int x)
	{
		nivelScene = x;
	}

	public void continuar()
	{	
		paused = false;
	}

	public void QuitGame()
	{
		Debug.Log("Exit");
		Application.Quit();
	}

	public void resetEscena()
	{
		Time.timeScale = 1;
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex,LoadSceneMode.Single);
	}

	public void siguienteEscena()
	{
		if(menuSalido)
		{
			Time.timeScale = 1;
			
			cambioNivelMusica = true;
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1,LoadSceneMode.Single);
		}
	}

	// Funcion para que el personaje vaya con la plataforma
	public void nuevoParent(Transform nuevoPadre,Transform objeto)
    {
    	objeto.SetParent(nuevoPadre);
		
    }

	//Funcion para que el personaje vuelva a ir solo con el nivel
	public void quitarParent(Transform propio)
    {
        propio.parent = nivel.transform;
	}
	public void encenderMenu()
	{
		Opciones.GetComponent<opciones>().nivelesAbiertos = SceneManager.GetActiveScene().buildIndex + 1 - 4;
		menuFinal.SetActive(true);
		iconoTiempo.SetActive(true);
		tiempoText.SetActive(true);
		if(colecionableCogido)
		{
			if(SceneManager.GetActiveScene().buildIndex == 4 || SceneManager.GetActiveScene().buildIndex == 7 || SceneManager.GetActiveScene().buildIndex == 11)
			{
				iconoColeccionable.SetActive(true);
				uiColeccionable.GetComponent<Animator>().SetBool("encontrado",true);
			}	
		}else
		{
			if(SceneManager.GetActiveScene().buildIndex == 4 || SceneManager.GetActiveScene().buildIndex == 7 || SceneManager.GetActiveScene().buildIndex == 11)
			{
				iconoColeccionable.SetActive(true);
				uiColeccionable.GetComponent<Animator>().SetBool("encontrado",false);	
			}
		}
		if(timeFinal < tiempoObjetivo[2])
		{
			estrella1.SetActive(true);
		}
		if(timeFinal < tiempoObjetivo[1])
		{
			estrella2.SetActive(true);
		}
		if(timeFinal < tiempoObjetivo[0])
		{
			estrella3.SetActive(true);
		}
		menuSalido = true;
	}

}
