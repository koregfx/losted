﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cinta_transportadora : MonoBehaviour 
{
private float scrollSpeed = 0.5f;

    void Update()
    {
        float offset = Time.time * scrollSpeed;
        gameObject.GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(0, offset ));
    }
}
