﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class botonesOpciones : MonoBehaviour 
{

	public GameObject Opciones;
	public bool ayudaCamino;
	public Toggle ayuda; 
	public Slider musica;
	void Start () 
	{
		Opciones = GameObject.FindGameObjectWithTag("opciones");
		musica.value = Opciones.GetComponent<opciones>().volMusic;
		ayuda.isOn = Opciones.GetComponent<opciones>().ayudaFlecho;
	}
	
	
	void Update ()
	{
		Opciones.GetComponent<opciones>().ayudaFlecho = ayuda.isOn;
		Opciones.GetComponent<opciones>().volMusic = musica.value;
	}
}
