﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class creditos : MonoBehaviour 
{

	public bool inicio;
	public float velocidad;
	public float limite;
	void Start () 
	{
		inicio = false;
		StartCoroutine(inicioCreditos());
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(inicio == true)
		{
			this.gameObject.GetComponent<RectTransform>().anchoredPosition += new Vector2(0, 1) * velocidad* Time.deltaTime; 
		
		}
	}
	IEnumerator inicioCreditos()
	{
		yield return new WaitForSeconds(2f);
		inicio = true;
		yield return new WaitForSeconds(limite);
		SceneManager.LoadScene(0, LoadSceneMode.Single);
	}
}
