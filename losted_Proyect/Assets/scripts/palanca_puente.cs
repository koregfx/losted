﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class palanca_puente : MonoBehaviour 
{
	private GameObject palanca;
	private GameObject player;
	private GameObject puntos;
	private GameObject cubos;
	private int numCubos;
	public bool activado;
	private float distancePlaPal;
	public float distanRec;
	public float vel;
	private Vector3[] posicioninic;
	void Start () 
	{
		player = GameObject.FindGameObjectWithTag("playerpos");
		palanca = this.gameObject.transform.GetChild(0).gameObject;
		cubos = this.gameObject.transform.GetChild(1).gameObject;
		puntos = this.gameObject.transform.GetChild(2).gameObject;
		numCubos = cubos.transform.childCount;
		activado = false;
		posicioninic = new Vector3[numCubos];
		for(int e = 0; e < numCubos; e++)
		{
			posicioninic[e] = cubos.transform.GetChild(e).transform.localPosition;
		}
	}
	void Update () 
	{
		distancePlaPal = Vector3.Distance(palanca.transform.position , player.transform.position);

		if(distancePlaPal < distanRec)
		{
			if(Input.GetKeyDown(KeyCode.Space))
			{
				activado = !activado;
			}
		}
		if(activado)
		{
			for(int e = 0; e < numCubos; e++)
			{
				cubos.transform.GetChild(e).transform.localPosition = Vector3.Lerp(cubos.transform.GetChild(e).transform.localPosition, puntos.transform.GetChild(e).transform.localPosition, vel);
			}
			
		}else
		{
			for(int e = 0; e < numCubos; e++)
			{
				cubos.transform.GetChild(e).transform.localPosition = Vector3.Lerp(cubos.transform.GetChild(e).transform.localPosition, posicioninic[e], vel);
			}
		}
	}
}
