﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inicio_nivel : MonoBehaviour 
{
private int numCub;
private Color transp;
private float[] velocidad;
private Vector3[] posicionScape;
public float vel;
public float offset;
private GameObject hijo;
private Material materialAlpha;
public float velo;
private float guarda;
public float velrot;
public float limiteDestroy;
public float progresionVel;
void Start () 
	{
		numCub = gameObject.transform.childCount;
		velocidad = new float[numCub];
		posicionScape = new Vector3[numCub];	
		for(int i = 0; i<numCub; i++)
		{
			velocidad[i] = Random.value *vel + velo;
			posicionScape[i] = Random.insideUnitSphere * offset;
		}
		for(int i=0; i<numCub; i++)
        {
            for(int j=0; j<numCub-1; j++)
            {
                if(velocidad[j+1]<velocidad[j])
                {
                    guarda = velocidad[j+1];
                    velocidad[j+1] = velocidad[j];
                    velocidad[j] = guarda;
                }
            }
        }
	}
	void Update () 
	{
		numCub = gameObject.transform.childCount;
		for(int i = 0; i<numCub; i++)
		{
			hijo = gameObject.transform.GetChild(i).gameObject;
			hijo.gameObject.transform.Rotate(posicionScape[i] * velrot);
			hijo.GetComponent<Renderer>().material.color =new Color(hijo.GetComponent<Renderer>().material.color.r, hijo.GetComponent<Renderer>().material.color.g, hijo.GetComponent<Renderer>().material.color.b, hijo.GetComponent<Renderer>().material.color.a - velocidad[i]);
			hijo.gameObject.transform.position = Vector3.Lerp(hijo.gameObject.transform.position, posicionScape[i], velocidad[i]);
			if(hijo.GetComponent<Renderer>().material.color.a < limiteDestroy)
			{
				velocidad[i] += progresionVel;
			}
			if(hijo.GetComponent<Renderer>().material.color.a < 0)
			{
				Destroy(hijo);
			}
		}
		//Debug.Log("NUMBCUB : " + numCub);
		if(numCub == 0) 
		{
			//Debug.Log("ENTRADA");
			Destroy(this.gameObject);
		}
	}
}
