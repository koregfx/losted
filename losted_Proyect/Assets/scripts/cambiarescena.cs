﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class cambiarescena : MonoBehaviour
{
	public Material Salida;
	public bool acceso;
	public int ultimoNivel;
	//public GameObject birutita;
	public bool vfxactivator;
	void start()
	{
		acceso = false;
		vfxactivator = false;
	}
	void Update()
	{
		// momento en el que queremos que pueda pasar el nivel
		if(acceso == true)
		{
			
			this.gameObject.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().material = Salida;
		}
		if(vfxactivator)
		{
			//GameObject particulas = Instantiate(birutita, transform.position, Quaternion.identity );
			vfxactivator = false;
		}

	}
	void OnCollisionEnter (Collision collision)
	{
		if(collision.gameObject.tag == "Player" && acceso == true)
		{
			gameObject.GetComponent<AudioSource>().Play(0);
			transform.GetChild(2).gameObject.SetActive(true);
			gameMannager.Mannager.finNivel = true;
		}	
	}
}
