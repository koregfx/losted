﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class timeCounter : MonoBehaviour {


private Text timeText;
	void Start () 
	{
		timeText = GetComponent<Text>();		
	}
	
	void Update () 
	{
		timeText.text = gameMannager.Mannager.timeMin.ToString("00") + ":" + gameMannager.Mannager.timeSec.ToString("00");
	}
}
