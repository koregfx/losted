﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class giracion_mundo : MonoBehaviour 
{
	[Header("Objects para la rotacion del mundo")]
	public GameObject PuntoA;
	public GameObject PuntoB;
	public GameObject PuntoC;
	public GameObject PuntoD;
	public GameObject Objetivo;

	[Header("Variables para el control del mundo")]
	public float velocidadgiro;
	public float tiempoEspera;
	private bool girando;
	private float cuentaTiempo;
	private int Estado;
	void Start () 
	{
		Estado = 1 ;
		girando = false ;
		cuentaTiempo = 0;	
	}
	

	void Update () 
	{
		//rotacion del mundo
		transform.LookAt(Objetivo.transform.position);

		//cambios de estado del giro
		if(gameMannager.Mannager.enJuego)
		{
			if(Input.GetKeyDown(KeyCode.E) && girando == false)
			{
				Estado++ ;
				if (Estado >=5)
				{
					Estado = 1;
				}
				girando = true;
			}
			if(Input.GetKeyDown(KeyCode.Q) && girando == false)
			{
				Estado-- ;
				if(Estado <=0)
				{
					Estado = 4;
				}
				girando = true;
			}
		}

		
		
		// condicion para no poder volver a girar hasta pasado el tiempo deseado
		if(girando == true)
		{
			cuentaTiempo += Time.deltaTime;

			if(cuentaTiempo >= tiempoEspera)
			{
				girando = false;
				cuentaTiempo = 0;
			}
		}

		//Definimos los estados
		if(Estado == 1)
		{
			Objetivo.transform.position = Vector3.Slerp(Objetivo.transform.position , PuntoA.transform.position , velocidadgiro * Time.deltaTime) ;
		}
		if(Estado == 2)
		{
			Objetivo.transform.position = Vector3.Slerp(Objetivo.transform.position , PuntoB.transform.position , velocidadgiro * Time.deltaTime) ;
		}
		if(Estado == 3)
		{
			Objetivo.transform.position = Vector3.Slerp(Objetivo.transform.position , PuntoC.transform.position , velocidadgiro * Time.deltaTime) ;
		}
		if(Estado == 4)
		{
			Objetivo.transform.position = Vector3.Slerp(Objetivo.transform.position , PuntoD.transform.position , velocidadgiro * Time.deltaTime) ;
		}
	}
}
