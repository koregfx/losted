﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class cubo_definitivo : MonoBehaviour 
{
private GameObject imageSpace;
private GameObject agarre;
private GameObject player;
private float distancePlayerToCube;
public float distanciaReconocimiento;
private bool estaCerca;
private bool cogido;
public bool puedeCogerse;
private float offset;
private bool repararCoger;

	void Start () 
	{
		//COGEMOS REFERENCIAS DE LOS OBJETOS NECESARIOS
		agarre = GameObject.FindGameObjectWithTag("coger");
		player = GameObject.FindGameObjectWithTag("playerpos");
		imageSpace = GameObject.FindGameObjectWithTag("spacecont");

		//INICIALIZACION DE LAS BOOLEANAS NECESARIAS EN FALSE
		estaCerca = false;
		cogido = false;
		puedeCogerse = false;
		repararCoger = false;

		//INICIALIZAR COROUTINAS
		StartCoroutine(comprobarDistaciaPlayer());

	}
	
	void Update () 
	{
		//SOLTAR EL CUBO
		if(Input.GetKeyDown(KeyCode.Space) && cogido && !repararCoger)
		{
			cogido = !cogido;
			repararCoger = true;
		}
		//COGER EL CUBO COMPROBANDO LA DISTANCIA Y SI LE MIRAN
		if(estaCerca && puedeCogerse && !repararCoger)
		{
			if(SceneManager.GetActiveScene().buildIndex == 4)
			{
				imageSpace.SetActive(true);
			}

			if(Input.GetKeyDown(KeyCode.Space))
			{
				cogido = !cogido;
				offset = transform.position.y - player.transform.position.y;
				repararCoger = true;
			}
		}else
		{
			if(SceneManager.GetActiveScene().buildIndex == 4)
			{
				imageSpace.SetActive(false);
			}
		}
		if(cogido)
		{
			coger();
		}	
		repararCoger = false;

	}

	IEnumerator comprobarDistaciaPlayer()
	{
		while(true)
		{
			//TRANSFORMANDO LA DISTACIA A EL PLANO (X,Y)
			Vector2 cubepos = new Vector2(transform.position.x ,transform.position.z);
			Vector2 playerpos = new Vector2(player.transform.position.x, player.transform.position.z);
			distancePlayerToCube = Vector2.Distance(playerpos,cubepos);

			if(distancePlayerToCube < distanciaReconocimiento)
			{
				estaCerca = true;
			}
			else
			{
				estaCerca = false;
			}
			yield return new WaitForSeconds (0.2f);
		}
	}

	private void coger()
	{
		this.transform.position = agarre.transform.position + new Vector3(0,offset,0);
		this.gameObject.GetComponent<Rigidbody>().isKinematic = false;
	}

	void OnCollisionEnter(Collision collision)
 	{
		if(collision.gameObject.tag == "plataforma")
		{		
    	gameMannager.Mannager.nuevoParent(collision.transform, transform);
		}
		
		if(collision.gameObject.tag == "escenario")
		{
			//volvemos a ponerle el kinematico
    		this.gameObject.GetComponent<Rigidbody>().isKinematic = true;
		}
		if(collision.gameObject.tag == "cuboMovil")
		{
			//volvemos a ponerle el kinematico
    		this.gameObject.GetComponent<Rigidbody>().isKinematic = true;
		}
		
 	}
	private void OnTriggerStay(Collider collision)
 	{
		if(collision.gameObject.tag == "posColocar" && !cogido)
		{		
			transform.position = new Vector3 (collision.transform.position.x, transform.position.y, collision.transform.position.z);
		}
		
 	}

	void OnCollisionExit(Collision collision)
  {
  	if(collision.gameObject.tag == "plataforma")
		{
			gameMannager.Mannager.quitarParent(transform);
		}
  }

}
