﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class controlador_personaje : MonoBehaviour
{
private float x;
private float y;
public float giroz;
public float velocidad;
private int escena;
private GameObject salida;
private GameObject llave;
public float velCinta;
public bool movVar;
private GameObject[] cubosMoviles;
private Animator anim;
public GameObject particulas;

	void Start () 
	{
		salida = GameObject.FindGameObjectWithTag("salida");
		llave = GameObject.FindGameObjectWithTag("llave");
		movVar = true;
		cubosMoviles = GameObject.FindGameObjectsWithTag("cuboMovil");
		anim = gameObject.transform.GetChild(0).GetChild(0).GetComponent<Animator>();
	}
	void Update () 
	{
		if(movVar)
		{
			if(!gameMannager.Mannager.finNivel)
			{
				x = Input.GetAxis("Vertical") * velocidad;
				y = Input.GetAxis("Horizontal") * giroz;
				//Moviemiento hacia alante y atras
				transform.Translate(Vector3.forward * x * Time.deltaTime);
		
				//Rotacion del cuerpo del personaje
				transform.Rotate(transform.up * y * Time.deltaTime,Space.World);
			}
		}else
		{
			if(!gameMannager.Mannager.finNivel)
			{
				x = Input.GetAxis("Vertical") * velocidad;
				y = Input.GetAxis("Horizontal") * velocidad;
				//Moviemiento hacia alante y atras
				transform.Translate(Vector3.forward * x * Time.deltaTime);
				//Moviemiento hacia alante y atras
				transform.Translate(Vector3.right * y * Time.deltaTime);
			}
		}



		
		//REINICIO DEL NIVEL

		escena = SceneManager.GetActiveScene().buildIndex;
		if(this.gameObject.transform.position.y <-20)
		{
			SceneManager.LoadScene(escena,LoadSceneMode.Single);
		}

		//COGER CUBO
		if(Input.GetKeyDown(KeyCode.Space))
		{	
			cogerCubo();
		}

		//ANIMACION
		anim.SetFloat("desplazamiento", x);

	}

	
	void OnCollisionStay(Collision collision)
  {
		
		if(collision.gameObject.tag == "plataforma")
		{
      gameMannager.Mannager.nuevoParent(collision.transform, transform);
		}
  }
	void OnCollisionExit(Collision collision)
  {
    if(collision.gameObject.tag == "plataforma")
		{
			gameMannager.Mannager.quitarParent(transform);
		}
  }


	void OnTriggerEnter(Collider collider)
	{
		if(collider.gameObject.tag == "llave")
		{ 
			
			salida.GetComponent<cambiarescena>().acceso = true;
			salida.GetComponent<cambiarescena>().vfxactivator = true;
			GameObject cosa = Instantiate(particulas, collider.transform.position, Quaternion.identity );
			cosa.transform.SetParent(gameMannager.Mannager.nivel.transform);
			Destroy(cosa,3f);
			Destroy(collider.gameObject);
		}
		if(collider.gameObject.tag == "coleccionable")
		{ 
			gameObject.GetComponent<AudioSource>().Play(0);
			gameMannager.Mannager.colecionableCogido = true;
			Destroy(collider.gameObject);
		}
		
	}
	void OnTriggerStay(Collider collider)
	{
		if(collider.gameObject.tag == "cinta")
		{
			
			this.gameObject.transform.Translate(collider.gameObject.transform.forward * velCinta * Time.deltaTime, Space.World);
		}
	}
	void cogerCubo()
	{
		RaycastHit cubo;
		if(Physics.Raycast(transform.position, transform.forward, out cubo, 10f))
		{	
			if(cubo.transform.gameObject.tag == "cuboMovil")
			{
				cubo.transform.gameObject.GetComponent<cubo_definitivo>().puedeCogerse = true;
				anim.SetTrigger("coger");
			}
		}
		else
		{
			for(int i = 0; i<cubosMoviles.Length; i++)
			{
				cubosMoviles[i].gameObject.GetComponent<cubo_definitivo>().puedeCogerse = false;
			}
		}
	}
}
