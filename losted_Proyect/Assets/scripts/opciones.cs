﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class opciones : MonoBehaviour
{
   
     private static opciones instance = null;
    public static opciones Instance
    {
        get { return instance; }
    }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }
	public bool ayudaFlecho = true;
	public float volMusic;
    public int nivelesAbiertos;
    
    void Start()
    {
        nivelesAbiertos = -1;
        volMusic = 1;
        ayudaFlecho = true;
    }
  
}