﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class plataforma : MonoBehaviour 
{
public float mov;
private bool change;
private GameObject objetivo;
public GameObject punto1;
public GameObject punto2;
public float tiempoEspera;
private float tiempoAcumulado;

	void Start()
	{
		objetivo = punto2;
		change = false;
	}
	void Update () 
	{
		//Movimiento de la plataforma
		//Limitaciones
		transform.position = Vector3.Lerp(transform.position, objetivo.transform.position , mov * Time.deltaTime );
		
		if(Vector3.Distance(transform.position, objetivo.transform.position)<0.2f)
		{
			cuentaTiempo();
			if(tiempoAcumulado >= tiempoEspera)
			{
				tiempoAcumulado = 0;
				if(string.Compare(objetivo.name, punto1.name)== 0)
				{
					objetivo = punto2;
					change = true;
				}
				if(string.Compare(objetivo.name, punto2.name)== 0 && !change)
				{
					objetivo = punto1;
					change = true;
				}
			}
			
		}
		change = false;
	}
	private void cuentaTiempo()
	{
		tiempoAcumulado += Time.deltaTime;
	}
	
}
